<?php

namespace Drupal\humanitix_api\Client;

use Drupal\Core\Cache\CacheBackendInterface;
use Drupal\humanitix_api\HttpClientInterface;

/**
 * The cached HTTP client.
 */
final class CachedHttpClient implements HttpClientInterface {

  /**
   * The Humanitix API client.
   *
   * @var \Drupal\humanitix_api\HttpClientInterface
   */
  protected $client;

  /**
   * The cache backend.
   *
   * @var \Drupal\Core\Cache\CacheBackendInterface
   */
  protected $cache;

  /**
   * The length of time for which a cached item should be valid.
   *
   * @var int
   */
  protected $cacheDuration;

  /**
   * {@inheritdoc}
   */
  public function __construct(HttpClientInterface $client, CacheBackendInterface $cache) {
    $this->client = $client;
    $this->cache = $cache;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(HttpClientInterface $client, CacheBackendInterface $cache) {
    return new self($client, $cache);
  }

  /**
   * Sets the cache duration.
   */
  public function setCacheDuration($duration) {
    $this->cacheDuration = $duration;
  }

  /**
   * {@inheritdoc}
   */
  public function makeRequest(
    string $verb,
    string $endpoint,
    array $params = [],
    string $body = '',
    array $headers = [],
    array $options = []) {
    $cid = hash('sha256', serialize(func_get_args()));

    if ($cached = $this->cache->get($cid)) {
      $result = unserialize($cached->data, ['allowed_classes' => FALSE]);
    }
    else {
      $result = $this->client->makeRequest($verb, $endpoint, $params, $body, $headers, $options);
      $cache_time = time() + $this->cacheDuration;
      $this->cache->set($cid, serialize($result), $cache_time);
    }

    return $result;
  }

}
