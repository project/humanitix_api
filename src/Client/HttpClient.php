<?php

namespace Drupal\humanitix_api\Client;

use Drupal\humanitix_api\HttpClientInterface;
use Drupal\humanitix_api\Humanitix;

/**
 * The HTTP client class.
 */
final class HttpClient extends Humanitix implements HttpClientInterface {

  /**
   * Create a new client instance.
   *
   * @param string $token
   *   The Humanitix OAuth token.
   *
   * @return static
   *   The HTTP client object.
   *
   * @throws \Exception
   */
  public static function create(string $token) {
    return new self($token);
  }

}
