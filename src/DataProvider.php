<?php

namespace Drupal\humanitix_api;

use Drupal\humanitix_api\Iterator\MappingIterator;
use Drupal\humanitix_api\Iterator\RequestIterator;
use Drupal\humanitix_api\Iterator\ResponseIterator;

/**
 * The Humanitix data provider.
 */
class DataProvider implements DataProviderInterface {

  /**
   * The Humanitix API client.
   *
   * @var \Drupal\humanitix_api\HttpClientInterface
   */
  protected $client;

  /**
   * HTTP methods that this class can currently handle.
   *
   * @var array
   */
  protected static $implementedMethods = [
    'GET',
  ];

  /**
   * Mapping of types to endpoints.
   *
   * @var array
   */
  protected static $endpoints = [
    'event' => 'events/:id',
    'tag'   => 'tags/:id',
  ];

  /**
   * Mapping of types to their collection endpoints.
   *
   * @var array
   */
  protected static $multipleEndpoints = [
    'event' => 'events',
    'tag' => 'tags',
  ];

  /**
   * The constructor method.
   */
  public function __construct(HttpClientInterface $http_client) {
    $this->client = $http_client;
  }

  /**
   * {@inheritdoc}
   */
  public function get(string $type, string $id, array $query = []) {
    $endpoint = $this->getEndpoint('GET', $type);

    if (str_contains($endpoint, ':id')) {
      $endpoint = str_replace(':id', $id, $endpoint);
    }

    if ($endpoint) {
      if ($data = $this->getData($this->makeRequest('GET', $endpoint, $query))) {
        return $this->getTypedData($data);
      }
    }

    return NULL;
  }

  /**
   * {@inheritdoc}
   */
  public function getMultiple(string $type, array $query = [], int $offset = 0, int $count = -1) {
    $endpoint = $this->getMultipleEndpoint('GET', $type);

    if ($endpoint) {
      $client = $this->client;
      // Get an iterator over paginated requests.
      $requests = RequestIterator::create($client, 'GET', $endpoint, $query);
      // Cache the API requests for the duration of this script.
      $cached = new \CachingIterator($requests, \CachingIterator::TOSTRING_USE_KEY);
      // Map the responses to TypedData.
      $mapped = MappingIterator::create(
        // Create an iterator over the results of each request.
        ResponseIterator::create($type, $cached),
        // Maps the raw response data into TypedData.
        function ($data) {
          return $this->getTypedData($data);
        }
      );

      return new \LimitIterator($mapped, $offset, $count);
    }

    return new \EmptyIterator();
  }

  /**
   * Wraps call to client makeRequest.
   *
   * @param string $method
   *   The HTTP method.
   * @param string $endpoint
   *   The API endpoint.
   * @param array $params
   *   The params array.
   *
   * @return array|mixed|\Psr\Http\Message\ResponseInterface
   *   The HTTP responce object.
   *
   * @throws \Exception
   */
  protected function makeRequest(string $method, string $endpoint, array $params = []) {
    return $this->client->makeRequest($method, $endpoint, $params);
  }

  /**
   * Get the endpoint for a specific type.
   *
   * @param string $method
   *   The HTTP method.
   * @param string $type
   *   The data type for which to request data.
   *
   * @return string|null
   *   The endpoint path or NULL if one does not exist.
   */
  protected function getEndpoint($method, $type) {
    $methods = self::$implementedMethods;
    $types = array_keys(self::$endpoints);

    if (in_array($method, $methods) && in_array($type, $types)) {
      return self::$endpoints[$type];
    }

    return $type;
  }

  /**
   * Extracts the raw data from the client response.
   *
   * $param array $response
   *   The raw client response.
   */
  protected function getData($response) {
    $code = (integer) $response['code'];
    // If the request was successful.
    if ($code >= 200 && $code < 300) {
      return $response['body'] ?? NULL;
    }

    return NULL;
  }

  /**
   * Gets the typed data.
   */
  protected function getTypedData($data) {
    if (!$data) {
      return NULL;
    }
    // @todo make this TypedData
    return $data;
  }

  /**
   * Get the endpoint for a specific type.
   *
   * Until more endpoints are supported, if the $type is not found,
   * it is simply returned as the endpoint value. This should be treated as
   * unstable behavior.
   *
   * @param string $method
   *   The HTTP method.
   * @param string $type
   *   The data type for which to request data.
   *
   * @return string|null
   *   The endpoint path or NULL if one does not exist.
   */
  protected function getMultipleEndpoint($method, $type) {
    $methods = self::$implementedMethods;
    $types = array_keys(self::$multipleEndpoints);

    if (in_array($method, $methods) && in_array($type, $types)) {
      return self::$multipleEndpoints[$type];
    }

    return $type;
  }

}
