<?php

namespace Drupal\humanitix_api;

/**
 * The HTTP client factory.
 */
interface HttpClientInterface {

  /**
   * A slightly abstracted wrapper around call().
   *
   * This essentially splits the call options array into different parameters
   * to make it more obvious to less advanced users what parameters can be
   * passed to the client.
   *
   * @param string $verb
   *   The HTTP method.
   * @param string $endpoint
   *   The HTTP API endpoint.
   * @param array $params
   *   The HTTP parameters array.
   * @param string $body
   *   The body array.
   * @param array $headers
   *   The headers array.
   * @param array $options
   *   The options array.
   *
   * @return array|mixed|\Psr\Http\Message\ResponseInterface
   *   The response object.
   *
   * @throws \Exception
   */
  public function makeRequest(
    string $verb,
    string $endpoint,
    array $params = [],
    string $body = '',
    array $headers = [],
    array $options = []);

}
