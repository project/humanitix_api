<?php

namespace Drupal\humanitix_api;

use GuzzleHttp\Client;
use GuzzleHttp\Exception\BadResponseException;
use GuzzleHttp\Psr7\Request;
use Psr\Http\Message\ResponseInterface;

/**
 * The Humanitix API HTTP client file.
 *
 * @method get(array $args)
 */
class Humanitix {

  /**
   * The current version of this library.
   */
  const VERSION = '1.0.1';

  /**
   * The API endpoint to get the current user's details.
   *
   * @var string
   */
  const EVENTS_ENDPOINT = 'events';

  /**
   * The token key.
   *
   * @var string
   */
  private $token;

  /**
   * The HTTP client.
   *
   * @var \GuzzleHttp\Client
   */
  private $client;

  /**
   * The last request object.
   *
   * @var \GuzzleHttp\Psr7\Request
   */
  private $lastRequest = NULL;

  /**
   * The last response object.
   *
   * @var \Psr\Http\Message\ResponseInterface
   */
  private $lastResponse = NULL;

  /**
   * The constructor for Humanitix class.
   *
   * @param string $token
   *   The OAuth token to authenticate the request.
   * @param array $config
   *   An array of Guzzle config options so that everything is configurable.
   *
   * @throws \Exception
   */
  public function __construct(string $token, array $config = []) {
    $default_config = [
      'base_uri' => 'https://api.humanitix.com/v1/',
      // Turn exceptions off so we can handle the responses ourselves.
      'exceptions' => FALSE,
      'timeout' => 30,
    ];

    $config = array_merge($default_config, $config);
    // Add this last so it's always there and isn't overwritten.
    if (!empty($token)) {
      $this->token = $token;
      // Set the authorisation header.
      $config['headers']['x-api-key'] = $this->token;
      $this->client = new Client($config);
    }
    else {
      throw new \Exception('An OAuth token is required to connect to the Eventbrite API.');
    }
  }

  /**
   * Make the call to Eventbrite, only synchronous calls at present.
   *
   * @param string $verb
   *   The HTTP method.
   * @param string $endpoint
   *   The API endpoint.
   * @param array $options
   *   The options array.
   *
   * @return array|mixed|\Psr\Http\Message\ResponseInterface
   *   The response object.
   *
   * @throws \Exception
   */
  public function call(string $verb, string $endpoint, array $options = []) {
    if ($this->validMethod($verb)) {
      // Get the headers and body from the options.
      $headers = $options['headers'] ?? [];
      $body = $options['body'] ?? NULL;
      $pv = $options['protocol_version'] ?? '1.1';
      // Make the request.
      $request = new Request($verb, $endpoint, $headers, $body, $pv);
      // Save the request as the last request.
      $this->lastRequest = $request;
      // Send it.
      $response = $this->client->send($request, $options);
      if ($response instanceof ResponseInterface) {
        // Set the last response.
        $this->lastResponse = $response;
        // If the caller wants the raw response, give it to them.
        if (isset($options['parse_response']) && $options['parse_response'] === FALSE) {
          return $response;
        }
        $parsed_response = $this->parseResponse($response);
        return $parsed_response;
      }
      else {
        // This only really happens when the network is interrupted.
        throw new BadResponseException('A bad response was received.',
          $request);
      }
    }
    else {
      throw new \Exception('Unrecognised HTTP verb.');
    }
  }

  /**
   * A slightly abstracted wrapper around call().
   *
   * This essentially splits the call options array into different parameters
   * to make it more obvious to less advanced users what parameters can be
   * passed to the client.
   *
   * @param string $verb
   *   The HTTP method.
   * @param string $endpoint
   *   The HTTP API endpoint.
   * @param array $params
   *   The HTTP parameters array.
   * @param string $body
   *   The body.
   * @param array $headers
   *   The headers array.
   * @param array $options
   *   The options array.
   *
   * @return array|mixed|\Psr\Http\Message\ResponseInterface
   *   The response object.
   *
   * @throws \Exception
   */
  public function makeRequest(
    string $verb,
    string $endpoint,
    array $params = [],
    string $body = '',
    array $headers = [],
    array $options = []) {

    if ($body !== NULL) {
      $options['body'] = $body;
    }

    // Merge the mergeable arrays if necessary.
    $mergeable = [
      'query' => $params,
      'headers' => $headers,
    ];

    foreach ($mergeable as $key => $value) {
      if ($value !== NULL) {
        if (!isset($options[$key])) {
          $options[$key] = [];
        }
        $options[$key] = array_merge($options[$key], $value);
      }
    }

    // Make the call.
    return $this->call($verb, $endpoint, $options);
  }

  /**
   * Checks if the HTTP method being used is correct.
   *
   * @param string $http_method
   *   The HTTP method.
   *
   * @return bool
   *   The returned value.
   */
  public function validMethod(string $http_method) {
    if (in_array(strtolower($http_method),
      ['get', 'post', 'put', 'patch', 'delete'])) {
      return TRUE;
    }

    return FALSE;
  }

  /**
   * Parses the response from.
   *
   * @param \Psr\Http\Message\ResponseInterface $response
   *   The response object.
   *
   * @return array
   *   The returned array value.
   */
  public function parseResponse(ResponseInterface $response) {
    $body = $response->getBody()->getContents();

    return [
      'code' => $response->getStatusCode(),
      'headers' => $response->getHeaders(),
      'body' => ($this->isValidJson($body)) ? json_decode($body,
        TRUE) : $body,
    ];
  }

  /**
   * Checks a string to see if it's JSON. True if it is, false if it's not.
   *
   * @param string $string
   *   The JSON string.
   *
   * @return bool
   *   The returned value.
   */
  public function isValidJson($string) {
    if (is_string($string)) {
      json_decode($string);
      return (json_last_error() === JSON_ERROR_NONE);
    }
    return FALSE;
  }

  /**
   * Checks if the class can connect to the Eventbrite API.
   *
   * Checks if we can connect to the API by calling the user endpoint and
   * checking the response code. If the response code is 2xx it returns true,
   * otherwise false.
   *
   * @return bool
   *   The returned value.
   */
  public function canConnect() {
    $data = $this->get(self::EVENTS_ENDPOINT);
    if (str_starts_with($data['code'], '2')) {
      return TRUE;
    }
    return FALSE;
  }

  /**
   * Provides shortcut methods named by HTTP verbs.
   *
   * Provides shortcut methods for GET, POST, PUT, PATCH and DELETE.
   *
   * @param string $method
   *   The HTTP method.
   * @param array $args
   *   The args array.
   *
   * @return array|mixed|\Psr\Http\Message\ResponseInterface
   *   The resoncer object.
   *
   * @throws \Exception
   */
  public function __call(string $method, array $args) {
    if ($this->validMethod($method)) {
      array_unshift($args, $method);
      return call_user_func_array([$this, 'makeRequest'], $args);
    }
    else {
      throw new \BadMethodCallException('Method not found in class.');
    }
  }

  /**
   * Returns the last request object for inspection.
   *
   * @return \GuzzleHttp\Psr7\Request
   *   The request object.
   */
  public function getLastRequest() {
    return $this->lastRequest;
  }

  /**
   * Returns the last response object for inspection.
   *
   * @return \Psr\Http\Message\ResponseInterface
   *   The resopnce object.
   */
  public function getLastResponse() {
    return $this->lastResponse;
  }

}
