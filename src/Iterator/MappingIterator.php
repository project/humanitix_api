<?php

namespace Drupal\humanitix_api\Iterator;

/**
 * The mapping iterator class.
 */
final class MappingIterator extends \IteratorIterator {

  /**
   * The callback mapper.
   *
   * @var callable
   */
  protected $mapper;

  /**
   * The class constructor.
   *
   * @param \Iterator $iterator
   *   The iterator object.
   * @param callable $mapper
   *   The callable mapper.
   */
  public function __construct(\Iterator $iterator, callable $mapper) {
    parent::__construct($iterator);
    $this->mapper = $mapper;
  }

  /**
   * The creator function for the mapping iterator.
   */
  public static function create(\Iterator $iterator, callable $mapper) {
    return new self($iterator, $mapper);
  }

  /**
   * Returns the current iterator.
   *
   * @return callback
   *   The callback iterator function.
   */
  #[\ReturnTypeWillChange]
  public function current() {
    return call_user_func($this->mapper, parent::current());
  }

}
