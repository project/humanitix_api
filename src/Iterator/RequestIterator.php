<?php

namespace Drupal\humanitix_api\Iterator;

use Drupal\humanitix_api\HttpClientInterface;

/**
 * The request iterator class.
 */
final class RequestIterator implements \Iterator {

  /**
   * The Eventbrite API client.
   *
   * @var \Drupal\humanitix_api\HttpClientInterface
   */
  protected HttpClientInterface $client;

  /**
   * The HTTP method of the request.
   *
   * @var string
   */
  protected string $method;

  /**
   * The endpoint over which to iterate.
   *
   * @var string
   */
  protected string $endpoint;

  /**
   * Custom query parameters for the requests.
   *
   * @var array
   */
  protected array $query;

  /**
   * The current page.
   *
   * @var int
   */
  protected int $page;

  /**
   * The total pages available.
   *
   * @var int
   */
  protected int $pageCount;

  /**
   * The current response.
   *
   * @var array
   */
  protected array $response;

  /**
   * The constructor for request iterator.
   *
   * @param \Drupal\humanitix_api\HttpClientInterface $http_client
   *   The HTTP client.
   * @param string $method
   *   The method name.
   * @param string $endpoint
   *   The HTTP API endpoint.
   * @param array $query
   *   The query params array.
   */
  public function __construct(HttpClientInterface $http_client, string $method, string $endpoint, array $query) {
    $this->client = $http_client;
    $this->method = $method;
    $this->endpoint = $endpoint;
    $this->query = $query;
  }

  /**
   * The creator function for request iterator.
   *
   * @param \Drupal\humanitix_api\HttpClientInterface $http_client
   *   The HTTP client.
   * @param string $method
   *   The HTTP method.
   * @param string $endpoint
   *   The HTTP API endpoint.
   * @param array $query
   *   The query params array.
   *
   * @return static
   */
  public static function create(HttpClientInterface $http_client, string $method, string $endpoint, array $query) {
    return new self($http_client, $method, $endpoint, $query);
  }

  /**
   * Returns the current position.
   *
   * @return mixed
   *   The current item in iterator.
   */
  #[\ReturnTypeWillChange]
  public function current() {
    return $this->response;
  }

  /**
   * Gets the position.
   *
   * @return bool|float|int|mixed|string|null
   *   The iterator position.
   */
  #[\ReturnTypeWillChange]
  public function key() {
    return $this->page - 1;
  }

  /**
   * Returns the next iterator index.
   *
   * @throws \Exception
   */
  #[\ReturnTypeWillChange]
  public function next() {
    $this->page += 1;
    if ($this->validPage($this->page)) {
      $this->setResponse($this->page);
    }
    else {
      $this->response = [];
    }
  }

  /**
   * Rewinds the iterator.
   *
   * @throws \Exception
   */
  #[\ReturnTypeWillChange]
  public function rewind() {
    $this->page = 1;
    $this->setResponse($this->page);
  }

  /**
   * Checks it iterator is valid.
   *
   * @return bool
   *   Indicates whether the page is valid or not.
   */
  #[\ReturnTypeWillChange]
  public function valid() {
    if (!$this->validPage($this->page)) {
      return FALSE;
    }
    $code = $this->response['code'];
    return $code >= 200 && $code < 300;
  }

  /**
   * Sets the pager number.
   *
   * @param int $page
   *   The page number.
   *
   * @throws \Exception
   */
  protected function setResponse(int $page) {
    $query = is_array($this->query) ? $this->query : [];
    $params = array_merge($query, ['page' => $page]);
    $this->response = $this->client->makeRequest(
      $this->method,
      $this->endpoint,
      $params
    );

    $this->setPageCount($this->response);
  }

  /**
   * Sets the page count.
   *
   * @param array $response
   *   The current response.
   */
  protected function setPageCount(array $response) {
    if (isset($response['body'])) {
      $total = $response['body']['total'];
      $pageSize = $response['body']['pageSize'];
      $this->pageCount = ceil($total / $pageSize);
    }
  }

  /**
   * Checks if a page number is valid.
   *
   * @param int $page
   *   The pager page param.
   *
   * @return bool
   *   Indicates the valid/invalid result.
   */
  protected function validPage(int $page) {
    return !(!is_null($this->pageCount)) || $page <= $this->pageCount;
  }

}
