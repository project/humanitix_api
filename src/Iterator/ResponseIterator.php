<?php

namespace Drupal\humanitix_api\Iterator;

/**
 * The response iterator class.
 */
final class ResponseIterator implements \Iterator {

  /**
   * The current page.
   *
   * @var int
   */
  protected $position;

  /**
   * The Eventbrite API type.
   *
   * @var string
   */
  protected $type;

  /**
   * The underlying RequestIterator.
   *
   * @var \Iterator
   */
  protected $requestIterator;

  /**
   * The item iterator.
   *
   * @var \Iterator
   */
  protected $iterator;

  /**
   * The constructor method.
   *
   * @param string $type
   *   The iterator type.
   * @param \Iterator $iterator
   *   The iterator object.
   */
  public function __construct(string $type, \Iterator $iterator) {
    $this->type = $type;
    $this->requestIterator = $iterator;
    $this->position = 0;
  }

  /**
   * Creates new iterator.
   *
   * @param string $type
   *   The endpoint type.
   * @param \Iterator $iterator
   *   The iterator object.
   *
   * @return ResponseIterator
   *   The response iterator.
   */
  public static function create(string $type, \Iterator $iterator) {
    return new self($type, $iterator);
  }

  /**
   * Returns the current position.
   *
   * @return mixed
   *   The current item in iterator.
   */
  #[\ReturnTypeWillChange]
  public function current() {
    return $this->iterator->current();
  }

  /**
   * Gets the position.
   *
   * @return bool|float|int|mixed|string|null
   *   The iterator position.
   */
  #[\ReturnTypeWillChange]
  public function key() {
    return $this->position;
  }

  /**
   * Returns the next iterator index.
   */
  #[\ReturnTypeWillChange]
  public function next() {
    $this->position += 1;
    $this->iterator->next();
    if (!$this->iterator->valid()) {
      $this->requestIterator->next();
      $this->updateIterator();
    }
  }

  /**
   * Rewinds the iterator.
   */
  #[\ReturnTypeWillChange]
  public function rewind() {
    $this->position = 0;
    $this->requestIterator->rewind();
    $this->updateIterator();
  }

  /**
   * Checks it iterator is valid.
   */
  #[\ReturnTypeWillChange]
  public function valid() {
    return $this->iterator->valid();
  }

  /**
   * Updates the iterator with response data.
   */
  protected function updateIterator() {
    if ($this->requestIterator->valid()) {
      $response = $this->requestIterator->current();
      $this->iterator = $this->getIterator($response);
    }
    else {
      $this->iterator = new \EmptyIterator();
    }
  }

  /**
   * Gets the iterator based on the data type.
   *
   * @param array $response
   *   The response array.
   *
   * @return \ArrayIterator
   *   The iterator object.
   */
  protected function getIterator(array $response) {
    switch ($this->type) {
      case 'event':
        return new \ArrayIterator($response['body']['events']);

      case 'tag':
        return new \ArrayIterator($response['body']['tags']);

      default:
        return new \ArrayIterator($response['body']);
    }
  }

}
